#  Copyright (c) 2020 Kevin Fox
#
#  This file is part of FFXIV Character Creation Monitor.
#
#       FFXIV Character Creation Monitor is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       FFXIV Character Creation Monitor is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with FFXIV Character Creation Monitor.  If not, see <https://www.gnu.org/licenses/>.

from ccrm import ffxiv_http
# parsing, worlds

if __name__ == "__main__":
    # Access the web page
    ffxiv_api = ffxiv_http.FFXIVAPI()
    ffxiv_api.get_webpage_html()

    # Check the http response
    if ffxiv_api.http_response_code != 200:
        exit("HTTP GET ERROR")

    # Parse the http response
    ffxiv_api.get_world_statuses()
    print(ffxiv_api.statuses)
    for world in ffxiv_api.statuses:
        print(f"{world}: {ffxiv_api.statuses[world]}")

