#  Copyright (c) 2020 Kevin Fox
#
#  This file is part of FFXIV Character Creation Monitor.
#
#       FFXIV Character Creation Monitor is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       FFXIV Character Creation Monitor is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with FFXIV Character Creation Monitor.  If not, see <https://www.gnu.org/licenses/>.
"""http get and parsing

Author: Kevin Fox

This module contains the class FFXIVAPI, which is used to execute
an HTTP GET against the Final Fantasy XIV Character Creation
Restriction website. It also contains methods to parse the downloaded
HTML to get the status of each world

Classes:
    - FFXIVAPI
"""
# Imports: Standard Library
# -------------------------
from urllib import request


class FFXIVAPI:
    """Class to interact with an act upon FFXIV character creation webpage

    """
    FFXIV_URL = "https://na.finalfantasyxiv.com/lodestone/worldstatus/"
    HTML_CLASS_WORLDNAME = "world-list__world_name"
    HTML_PARAGRAPH_TAG = "<p>"

    def __init__(self):
        self.webpage_contents = None
        self.http_response_code = None
        self.statuses = {}

    def get_webpage_html(self):
        with request.urlopen(self.FFXIV_URL) as webpage_response:
            # Convert the HTTP GET response (bytes) to a string
            self.webpage_contents = webpage_response.read().decode("utf-8")
            # Split the string at the newline characters into a list
            self.webpage_contents = self.webpage_contents.splitlines()
            self.http_response_code = webpage_response.getcode()

    def get_world_statuses(self):
        # Initializations
        upcoming_world_name = False
        upcoming_world_status = False

        # Check if we actually have any html to parse
        if self.webpage_contents is None:
            return

        # Parse out the world statuses
        for line in self.webpage_contents:
            # Check if a world is in an upcoming tag
            if self.HTML_CLASS_WORLDNAME in line:
                upcoming_world_name = True

            elif upcoming_world_name:
                # Check to make sure we're on the right line
                if self.HTML_PARAGRAPH_TAG in line:
                    # Current line : <p>WorldName</p>
                    # Remove the leading/trailing whitespace
                    current_world = line.strip()
                    # Remove the paragraph tags
                    current_world = current_world[3:-4]

                    # The status of that world will be next
                    upcoming_world_name = False
                    upcoming_world_status = True

            elif upcoming_world_status:
                # Check to make sure we're on the right line
                if self.HTML_PARAGRAPH_TAG in line:
                    # Remove leading/trailing whitespace
                    current_status = line.strip()
                    # Remove the <p> and </p> tags
                    current_status = current_status[3:-4]
                    self.statuses[current_world] = current_status

                    upcoming_world_status = False
