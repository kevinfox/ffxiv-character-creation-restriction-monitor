#  Copyright (c) 2020 Kevin Fox
#
#  This file is part of FFXIV Character Creation Monitor.
#
#       FFXIV Character Creation Monitor is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       FFXIV Character Creation Monitor is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with FFXIV Character Creation Monitor.  If not, see <https://www.gnu.org/licenses/>.
"""Data structure with the various worlds and data centers

Author: Kevin Fox
"""


class Worlds:
    eu_chaos = [
        "Cerberus",
        "Louisoix",
        "Moogle",
        "Omega",
        "Ragnarok",
        "Spriggan"
    ]
    eu_light = [
        "Lich",
        "Odin",
        "Phoenix",
        "Shiva",
        "Twintania",
        "Zodiark"
    ]

    jp_elemental = [
        "Aegis",
        "Atomos",
        "Carbuncle",
        "Garuda",
        "Gungnir",
        "Kujata",
        "Ramuh",
        "Tonberry",
        "Typhon",
        "Unicorn"
    ]
    jp_gaia = [
        "Alexander",
        "Bahamut",
        "Durandal",
        "Fenrir",
        "Ifrit",
        "Ridill",
        "Tiamat",
        "Ultima",
        "Valefor",
        "Yojimbo",
        "Zeromus"
    ]
    jp_mana = [
        "Anima",
        "Asura",
        "Belias",
        "Chocobo",
        "Hades",
        "Ixion",
        "Mandragora",
        "Masamune",
        "Pandaemonium",
        "Shinryu",
        "Titan"
    ]

    na_aether = [
        "Adamantoise",
        "Cactuar",
        "Faerie",
        "Gilgamesh",
        "Jenova",
        "Midgardsormr",
        "Sargantanas",
        "Siren"
    ]
    na_primal = [
        "Behemoth",
        "Excalibur",
        "Exodus",
        "Famfrit",
        "Hyperion",
        "Lamia",
        "Leviathan",
        "Ultros"
    ]
    na_crystal = [
        "Balmung",
        "Brynhildr",
        "Coeurl",
        "Diabolos",
        "Goblin",
        "Malboro",
        "Mateus",
        "Zalera"
    ]

    eu_data_center = [
        eu_chaos,
        eu_light
    ]

    jp_data_center = [
        jp_elemental,
        jp_gaia,
        jp_mana
    ]
    na_data_center = [
        na_aether,
        na_crystal,
        na_primal
    ]
    data_centers = [
        eu_data_center,
        jp_data_center,
        na_data_center
    ]
