#!/bin/bash
# Author: Kevin Fox

############
## README ##
############
# Todo lol

###############
## Variables ##
###############
ffxivWorldStatusUrl=https://na.finalfantasyxiv.com/lodestone/worldstatus/
waitBetweenFetchInSeconds=30

###########################
## European Data Centers ##
###########################
Chaos=(
  Cerberus
  Louisoix
  Moogle
  Omega
  Ragnarok
)
Light=(
  Lich
  Odin
  Phoenix
  Shiva
  Zodiark
)
################################
# North American Data Centers ##
################################
Aether=(
  Adamantoise
  Cactuar
  Faerie
  Gilgamesh
  Jenova
  Midgardsormr
  Sargatanas
  Siren
)
Primal=(
  Behemoth
  Excalibur
  Exodus
  Famfrit
  Hyperion
  Lamia
  Leviathan
  Ultros
)
Crystal=(
  Balmung
  Brynhildr
  Coeurl
  Diabolos
  Goblin
  Malboro
  Mateus
  Zalera
)

###########################
## Japanese Data Centers ##
###########################
Elemental=(
  Aegis
  Atomos
  Carbuncle
  Garuda
  Gungnir
  Kujata
  Ramuh
  Tonberry
  Typhon
  Unicorn
)
Gaia=(
  Alexander
  Bahamut
  Durandal
  Fenrir
  Ifrit
  Ridill
  Tiamat
  Ultima
  Valefor
  Yojimbo
  Zeromus
)
Mana=(
  Anima
  Asura
  Belias
  Chocobo
  Hades
  Ixion
  Mandragora
  Masamune
  Pandaemonium
  Shinryu
  Titan
)
###########################
## Regional Data Centers ##
###########################
EU=(
  Chaos
  Light
)
NA=(
  Aether
  Primal
  Crystal
)
JP=(
  Elemental
  Gaia
  Mana
)
DATACENTERS=(
  EU
  NA
  JP
)

# Parse Command Line Args
if [ $# -gt 0 ]; then
  for arg in "$@"; do
    if [[ $arg == *"--region="* ]]; then
      echo "Matched region: TODO"
    elif [[ $arg == *"--datacenter="* ]]; then
      echo "Matched datacenter: TODO"
    elif [[ $arg == *"--world="* ]]; then
      echo "Matched world: TODO"
    fi 
  done  
fi

# Infinite loop of checking the ffxiv character creation website
while [ 1 ]; do
  # Grab the html
  ffxivHtml=$(curl -s $ffxivWorldStatusUrl)
  # Iterate through EU, NA, JP
  for regionalDataCenter in "${DATACENTERS[@]}"; do
    # Indirect array reference : refRegional has the contents of e.g. EU
    refRegional=$regionalDataCenter[@]
    for dataCenter in "${!refRegional}"; do
      # Indirect array reference : refDataCenter has the contents of e.g. Aether
      refDataCenter=$dataCenter[@]
      for world in "${!refDataCenter}"; do
	# Parsing HTML to see if our current world is available for character creation
	isWorldOpenForCharacterCreation=$( echo "$ffxivHtml" | grep -A 9 $world | grep Unavailable | wc -l)
	# isWorldOpenForCharacterCreation will be 0 if we don't see Unavailable (i.e. it is available)
	if [ $isWorldOpenForCharacterCreation -eq 0 ]; then
	  echo "$world is: Available"
	else
	 echo -e "\n$world is: UNAVAILABLE"
	fi
      done
    done
  done
  # Newline to separate cycles
  echo
  # Wait a bit of time here between fetching the HTML
  sleep $waitBetweenFetchInSeconds
done
