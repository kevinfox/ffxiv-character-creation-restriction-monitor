Author: Kevin Fox

Description:
------------
This script will continuous scan the Final Fantasy XIV Character Creation Restriction website, and output the status of which worlds are available

This is a bash script. It will run easily on Linux (and presumably Mac, but haven't tested that) as long as cURL is installed
To run on Windows, one can use Windows Subsystem for Linux, or try one of the many Bash terminals for Windows

There are two files:
*  curlFinalFantasyXiv.sh : This is the main script going forward, and the script that will be worked on
*  curlFinalFantasyXiv-simple.sh : This was the initial script, it will scan for a single world's status

Usage:
------
./curlFinalFantasyXiv.sh

History:
--------
Well this started as a joke because my buddy was waiting for Adamantoise to lift its character creation restriction.
So I jokingly made a script to monitor the website every x seconds with cURL, but it ended up being super useful.

Uploading it for the world. Initially it would only watch a single world, but added the function to monitor all worlds.

Next up:
--------
Working on command line args to specificy worlds, datacenters, or regions to watch