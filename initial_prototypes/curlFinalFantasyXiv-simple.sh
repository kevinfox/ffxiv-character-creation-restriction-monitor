#!/bin/bash
# Author: Kevin Fox

SERVER=Adamantoise
WAITSECONDS=30

# Infinite loop
while [ 1 ]; do
  # Check if the server still has character creation restrictions	
  isServerAvailable=$(curl -s https://na.finalfantasyxiv.com/lodestone/worldstatus/ | grep -A 9 $SERVER | grep Unavailable | wc -l) 
  if [ $isServerAvailable -eq 0 ]; then
	  echo -e "\n!!!! FUCK YEA !!!!\n"
  else
	  echo -e "\nawww, sadness consumes me\n"
  fi
  sleep $WAITSECONDS 
done
